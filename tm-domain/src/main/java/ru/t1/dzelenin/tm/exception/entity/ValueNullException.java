package ru.t1.dzelenin.tm.exception.entity;

public final class ValueNullException extends AbstractEntityException {

    public ValueNullException() {
        super("Error! This value is null...");
    }

}

